
import hk.quantr.javalib.CommonLib;
import java.io.File;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestClone {

	@Test
	public void test() throws GitAPIException {
		File dir = new File("test");
		CommonLib.deleteDirectory(dir);
		Git git = Git.cloneRepository()
				.setURI("https://gitlab.com/peter-example/java/jgit-example.git")
				.setDirectory(dir)
				.call();
	}
}
