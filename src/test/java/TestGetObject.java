
import hk.quantr.javalib.CommonLib;
import java.io.File;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestGetObject {

	@Test
	public void test() throws Exception {
		File dir = new File("test");
		CommonLib.deleteDirectory(dir);
		Git git = Git.cloneRepository()
				.setURI("https://gitlab.com/peter-example/java/jgit-example.git")
				.setDirectory(dir)
				.call();
		Repository repository = git.getRepository();
		ObjectId head = repository.resolve("HEAD");
		System.out.println(head);
		Ref HEAD = repository.getRef("refs/heads/master");
		System.out.println(HEAD);
	}
}
